Notifications Event README.txt

This is a plugin for the Notifications Module enabling you to send a mailout to notify users of upcoming events
It works for events created using both the event module and the calendar/cck module.  

Copy the Notifications Event directory to the Notifications Module directory and install.

Configuration:

Create time intervals to use with this module.  You will probably use different nomenclature: rather than 'weekly' you might prefer '1 week'.
Go to admin/messaging/notifications/calendar_events/general and configure for your own requirements.  You can either send mailouts via the cron run or as a batch run.
If you are using the event module you will be able to select among event content types to include.
If you are using the calendar module you will be able to select among date fields.

In the batch processes there is an option to subscribe all current users: admin/messaging/notifications/calendar_events/batch_processes

